import {Fournisseur} from './fournisseurInterface';

export interface Medicament {
    codeBarre: string;
    nom: string;
    quantite: Number;
    datePeremption: Date;
    prix : Number;
    fournisseur: Fournisseur
  }