export interface User {
    idUtilisateur: Number,
    nom: string,
    prenom: string,
    email: string,
    password :string,
    enable : boolean
}