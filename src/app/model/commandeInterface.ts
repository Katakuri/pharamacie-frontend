import { Medicament } from "./medicamentInterface";
import { Statut} from "./statutInterface";

export interface Commande {
    idCommande : Number,
    nbrMed : Number,
    medicament? : Medicament,
    montant : Number,
    dateCommande : Date,
    statut : Statut
}