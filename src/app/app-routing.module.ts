import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent } from '../app/modules/components/login/login.component'
import {ListeMedicamentsComponent} from "../app/modules/components/medicaments/liste-medicaments/liste-medicaments.component"
import { HomeComponent } from './modules/components/home/home/home.component';
import {AuthGuardService as AuthGuard} from "../../src/app/shared/services/auth-guard.service";
import {ListeCommandesComponent} from "../../src/app/modules/components/medicaments/commandes/liste-commandes/liste-commandes.component"
import { ListeFournisseursComponent } from './modules/components/fournisseurs/liste-fournisseurs/liste-fournisseurs.component';
import {RegisterComponent} from "../app/modules/components/register/register.component";
import { ProfilComponent } from './modules/components/profil/profil.component';

const routes: Routes = [
  { path : "", component: LoginComponent},
  { path : "listMedicaments", canActivate: [AuthGuard], component: ListeMedicamentsComponent },
  { path : "home", canActivate: [AuthGuard], component : HomeComponent},
  {path : "listCommandes", canActivate: [AuthGuard], component : ListeCommandesComponent},
  {path : "listFournisseur", canActivate: [AuthGuard], component: ListeFournisseursComponent},
  {path : "profil", component: ProfilComponent },
  {path : "register", component: RegisterComponent},
  {path : "**", redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
