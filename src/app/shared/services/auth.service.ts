import { Injectable } from '@angular/core';
import {User} from "../../model/userInterface";
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  url = "https://pharmacie-miage.herokuapp.com/"

  isLogged : boolean = false;

  constructor(
    private http: HttpClient
  ) { }

  register(user){
    return this.http.post(this.url + "register", user);
  }

  login(user){
    return this.http.post(this.url + 'login',user);
  }

  userProfil(){
    return this.http.get(this.url + "profil");
  }

  logout(){
    this.isLogged = false;
  }

  isUserLogged(){
    return localStorage.getItem('id_token') !== null;
  }
}
