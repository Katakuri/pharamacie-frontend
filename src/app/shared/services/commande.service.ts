import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class CommandeService {

  // url = "http://localhost:8080/";
  url = "https://pharmacie-miage.herokuapp.com/"

  constructor(
    private http: HttpClient
  ) { }

  //Nombre de commandes
  getNbCommandes(){
    return this.http.get(this.url + "countCommandes");
  }

  //Récupérer toutes les commandes
  getAllCommandes(){
    return this.http.get(this.url + "commandes");
  }

  //Supprimer une commande
  deleteCommande(idCommande){
    return this.http.delete( this.url + "commandes/" + idCommande)
  }

  //Nombre Commandes reçu
  getCommandesRecus(){
    return this.http.get( this.url + "NbrcommandesReçu")
  }

  // Changer le statut d'une commande "envoyée" à "reçue"
  updateStatutCommande(codeCommande, commande){
    return this.http.put(this.url + "commandes/" + codeCommande, commande);
  }

  // Changer le statut d'une commande "reçu" à "saisie"

  updateStatutCommandeSaisie(codeCommande, commande){
    return this.http.put(this.url + 'commandesReçu/' + codeCommande, commande);
  }


  // Commander un médciament
  addCommandeMedicament(codeMedicament, nbrMed){
    return this.http.post(this.url + "commandes/" + codeMedicament, nbrMed);
  }
}
