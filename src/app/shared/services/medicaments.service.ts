import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MedicamentsService {

  // url = "http://localhost:8080/";
  url = "https://pharmacie-miage.herokuapp.com/"

  constructor(
    private http: HttpClient
    ) { }

    //Récupere tout les médicaments
    getMedicaments() {
      return this.http.get<any>(this.url + "medicaments");
    }

    //Ajoute un médicament 
    addMedicament(medicament){
      return this.http.post(this.url + "addMedicament", medicament);
    }

    //Supprimer un médicament
    deleteMedicament(medicamentCode){
      return this.http.delete(this.url+ "medicaments/"+medicamentCode)
    }

    //Modifier un médicament
    updateMedicament(medicamentCodeBarre, medicament){
      return this.http.put( this.url+ "medicaments/"+ medicamentCodeBarre, medicament);
    }

    //Nombre médicaments date de péremption expirés
    getMedicamentExp(){
      return this.http.get(this.url + "nbrMedExp");
    }

    //Nombre de médicaments présent dans le stock
    getNbStock(){
      return this.http.get(this.url + "countmedicaments");
    }

    //Nombre de médicaments qui sont en dessous de la quantité minimale
    getNbMedicamentMin(){
      return this.http.get(this.url + "nbQuantiteMin")
    }

    //Liste médicaments avec date expirés
    getListMedicamentExpt(){
      return this.http.get(this.url + "medExp");
    }

    //Liste medicaments avec quantité minimale
    getListQuantiteMini(){
      return this.http.get(this.url + "quantiteMin");
    }
}
