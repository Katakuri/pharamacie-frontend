import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class FournisseurService {

  // url = "http://localhost:8080/";
  url = "https://pharmacie-miage.herokuapp.com/"

  constructor(
    private http: HttpClient
  ) { }

  //Liste de tous les fournisseurs
  getAllfournisseur(){
    return this.http.get(this.url + 'fournisseurs');
  }

  //Ajouter un fournisseur
  addFournisseur(newFournisseur){
    return this.http.post(this.url + "addFournisseur",newFournisseur);
  }

  //Contacter un fournisseur
  mailFournisseur(fournisseurid, mailContent){
    return this.http.post(this.url + "fournisseurs/"+ fournisseurid, mailContent);
  }
}
