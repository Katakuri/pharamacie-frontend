import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './modules/layouts/header/header.component';
import { FooterComponent } from './modules/layouts/footer/footer.component';
import { DemoMaterialModule } from './material-module';
import { LoginComponent } from './modules/components/login/login.component';
import { HttpClientModule } from '@angular/common/http';
import {ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ListeMedicamentsComponent } from './modules/components/medicaments/liste-medicaments/liste-medicaments.component';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import { DialogAddMedicamentComponent } from './modules/components/medicaments/liste-medicaments/dialogs-medicament/dialog-add-medicament/dialog-add-medicament.component';
import {MedicamentsService} from "./shared/services/medicaments.service";
import { DialogUpdateMedicamentComponent } from './modules/components/medicaments/liste-medicaments/dialogs-medicament/dialog-update-medicament/dialog-update-medicament.component';
import { DialogDeleteMedicamentComponent } from './modules/components/medicaments/liste-medicaments/dialogs-medicament/dialog-delete-medicament/dialog-delete-medicament.component';
import { HomeComponent } from './modules/components/home/home/home.component';
import {CommandeService} from './shared/services/commande.service';
import { ListeCommandesComponent } from './modules/components/medicaments/commandes/liste-commandes/liste-commandes.component';
import { DialogDeleteCommandeComponent } from './modules/components/medicaments/commandes/liste-commandes/dialog-commande/dialog-delete-commande/dialog-delete-commande.component';
import { DialogConfirmCommandeRecuComponent } from './modules/components/medicaments/commandes/liste-commandes/dialog-commande/dialog-confirm-commande-recu/dialog-confirm-commande-recu.component';
import {FournisseurService} from '../app/shared/services/fournisseur.service';
import { DialogCommanderMedicamentComponent } from './modules/components/medicaments/liste-medicaments/dialogs-medicament/dialog-commander-medicament/dialog-commander-medicament.component';
import { ListeFournisseursComponent } from './modules/components/fournisseurs/liste-fournisseurs/liste-fournisseurs.component';
import { DialogAddFournisseurComponent } from './modules/components/fournisseurs/liste-fournisseurs/dialog-fournisseur/dialog-add-fournisseur/dialog-add-fournisseur.component';
import { RegisterComponent } from './modules/components/register/register.component';
import { HttpClient, HTTP_INTERCEPTORS} from '@angular/common/http';
import { AuthInterceptor } from '../app/shared/services/interceptor.service';
import { ProfilComponent } from './modules/components/profil/profil.component';
import { DialogSendMailComponent } from './modules/components/fournisseurs/liste-fournisseurs/dialog-fournisseur/dialog-send-mail/dialog-send-mail.component';

registerLocaleData(localeFr, 'fr');

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    ListeMedicamentsComponent,
    DialogAddMedicamentComponent,
    DialogUpdateMedicamentComponent,
    DialogDeleteMedicamentComponent,
    HomeComponent,
    ListeCommandesComponent,
    DialogDeleteCommandeComponent,
    DialogConfirmCommandeRecuComponent,
    DialogCommanderMedicamentComponent,
    ListeFournisseursComponent,
    DialogAddFournisseurComponent,
    RegisterComponent,
    ProfilComponent,
    DialogSendMailComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    DemoMaterialModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [MedicamentsService, CommandeService, FournisseurService, { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }],
  entryComponents: [DialogAddMedicamentComponent, DialogUpdateMedicamentComponent, DialogDeleteMedicamentComponent,
    DialogDeleteCommandeComponent, DialogConfirmCommandeRecuComponent, DialogCommanderMedicamentComponent,
    DialogAddFournisseurComponent, DialogSendMailComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
