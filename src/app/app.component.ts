import { Component } from '@angular/core';
import { HeaderComponent } from '../app/modules/layouts/header/header.component';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'pharamcie-front';
}
