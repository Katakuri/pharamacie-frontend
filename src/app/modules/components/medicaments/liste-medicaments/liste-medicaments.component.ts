import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogAddMedicamentComponent } from './dialogs-medicament/dialog-add-medicament/dialog-add-medicament.component';
import { MedicamentsService } from "../../../../shared/services/medicaments.service";
import { DialogUpdateMedicamentComponent } from './dialogs-medicament/dialog-update-medicament/dialog-update-medicament.component';
import { DialogDeleteMedicamentComponent } from './dialogs-medicament/dialog-delete-medicament/dialog-delete-medicament.component';
import { ToolsService } from "../../../../shared/tools/tools.service"
import { Medicament } from "../../../../model/medicamentInterface"
import { DialogCommanderMedicamentComponent } from './dialogs-medicament/dialog-commander-medicament/dialog-commander-medicament.component';
import { CommandeService } from "../../../../shared/services/commande.service";
import { Router } from '@angular/router';

@Component({
  selector: 'app-liste-medicaments',
  templateUrl: './liste-medicaments.component.html',
  styleUrls: ['./liste-medicaments.component.scss']
})
export class ListeMedicamentsComponent implements AfterViewInit {
  displayedColumns: string[] = ['codeBarre', 'nom', 'datePeremption', 'fournisseur', 'quantite', 'prix', 'commander', 'modifier', 'supprimer'];
  dataSource: MatTableDataSource<Medicament>;
  tabMedicaments: any[] = [];

  tabMedicamentsExp: any[] = [];
  tabMedicamentsMin: any[] = [];


  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  ngOnInit() {
    this.medicamentService.getMedicaments().subscribe((res) => {
      console.log(res);
      this.tabMedicaments = res;
      this.dataSource = new MatTableDataSource(this.tabMedicaments);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

    },
      (err) => console.log(err))

    this.medicamentService.getListQuantiteMini().subscribe((res) => console.log(res),
      (err) => console.log(err));

    this.medicamentService.getListMedicamentExpt().subscribe((res) => console.log(res),
      (err) => console.log(err));

  }
  constructor(
    public dialog: MatDialog,
    public medicamentService: MedicamentsService,
    public tools: ToolsService,
    public commandeService: CommandeService,
    public router : Router
  ) {
  }

  ngAfterViewInit() {
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  openDialogAjoutMedicament() {
    let dialogRef = this.dialog.open(DialogAddMedicamentComponent, {
      width: "380px"
    }).afterClosed().subscribe((res) => {
      console.log(res);
      this.ngOnInit();
    },
      (err) => {
        console.log(err);
        this.ngOnInit();
      },
      () => this.ngOnInit());
  }

  openDialogUpdateMedicament(medicament) {
    let dialogRef = this.dialog.open(DialogUpdateMedicamentComponent, {
      width: "380px",
      data: medicament,
    }).afterClosed().subscribe((res: Medicament) => {
      console.log(res);
      let medicament: Medicament = res;
      this.medicamentService.updateMedicament(medicament.codeBarre ? medicament.codeBarre : '', medicament).subscribe((res) => {
        console.log(res);
        this.tools.openSnackBar("Le médicament a bien été modifié");
        this.ngOnInit();
      },
        (err) => {
          console.log(err);
          this.tools.openSnackBar(err.error.text);
          this.ngOnInit();
        })
    })
  }

  openDialogDeleteMedicament(medicament) {
    let dialogRef = this.dialog.open(DialogDeleteMedicamentComponent, {
      data: medicament
    }).afterClosed().subscribe((res) => {
      console.log(res);
      if (res) {
        this.medicamentService.deleteMedicament(medicament.codeBarre).subscribe(
          (res) => {
            this.tools.openSnackBar("Le médicament a bien été supprimé");
            this.ngOnInit();
          },
          (err) => this.tools.openSnackBar("Une erreur est survenu lors de la supression")
        )
      } else {
        this.tools.openSnackBar("Le medicament n'a pas été supprimé");
      }
    })
  }

  openDialogCommande(medicament) {
    let dialogRef = this.dialog.open(DialogCommanderMedicamentComponent, {
      data: medicament
    }).afterClosed().subscribe((res) => {
      if (res) {
        console.log(res);
        console.log(medicament.codeBarre);
        console.log(medicament.nbrMed)
        this.commandeService.addCommandeMedicament(medicament.codeBarre, res).subscribe((res) => {
          console.log(res);
          this.tools.openSnackBar("La commande a bien été envoyée");
        },
        (err) => {
          this.tools.openSnackBar("La commande a bien été envoyée");
          this.router.navigate(['/listCommandes'])
          console.log(err)
        },
        () => {
          this.tools.openSnackBar("La commande a bien été envoyée");
          this.router.navigate(['/listCommandes'])
        })
      } else {
        this.tools.openSnackBar("La commande n'a pas été envoyée")
      }
    })
  }

  listMedicamentsMini() {
    this.medicamentService.getListQuantiteMini().subscribe((res: any) => {
      console.log(res);
      this.tabMedicamentsMin = res;
      this.dataSource = new MatTableDataSource(this.tabMedicamentsMin);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },
      (err) => console.log(err));
  }

  listMedicamentExp() {
    this.medicamentService.getListMedicamentExpt().subscribe((res: any) => {
      console.log(res);
      this.tabMedicamentsExp = res;
      this.dataSource = new MatTableDataSource(this.tabMedicamentsExp);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    })
  }

  listAllMedicaments() {
    this.medicamentService.getMedicaments().subscribe(
      (res) => {
        this.tabMedicaments = res;
        this.dataSource = new MatTableDataSource(this.tabMedicaments);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    )
  }

}