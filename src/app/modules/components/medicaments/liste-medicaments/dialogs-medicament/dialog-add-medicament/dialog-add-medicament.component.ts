import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl} from '@angular/forms';
import {MatDialogRef} from '@angular/material';
import { Fournisseur } from 'src/app/model/fournisseurInterface';
import {MedicamentsService} from "../../../../../../shared/services/medicaments.service"
import {ToolsService} from "../../../../../../shared/tools/tools.service";
import {FournisseurService} from "../../../../../../shared/services/fournisseur.service"

@Component({
  selector: 'app-dialog-add-medicament',
  templateUrl: './dialog-add-medicament.component.html',
  styleUrls: ['./dialog-add-medicament.component.scss']
})
export class DialogAddMedicamentComponent implements OnInit {

  form : FormGroup;
  fournisseurs : Fournisseur[] = [];

  constructor(
    public dialogRef: MatDialogRef<DialogAddMedicamentComponent>,
    public medicamentService : MedicamentsService,
    public toolsService : ToolsService,
    public fournisService : FournisseurService
  ) { }

  ngOnInit() {
    this.fournisService.getAllfournisseur().subscribe((res : Fournisseur[]) => {
      console.log(res);
      this.fournisseurs = res;
    },
    (err) => console.log(err));
    
    this.form = new FormGroup({
      nom : new FormControl('',[Validators.required, Validators.maxLength(15)]), 
      quantite : new FormControl('', [Validators.required, Validators.maxLength(15)]),
      codeBarre : new FormControl('', [Validators.required, Validators.maxLength(15)]),
      datePeremption : new FormControl('', [Validators.required]),
      prix : new FormControl('',[Validators.required]),
      fournisseur : new FormControl('', [Validators.required])
    });
  }

  onNoClick(): void {
    this.dialogRef.close('KO');
  }

  submit(form){
    console.log(form.value);
    this.medicamentService.addMedicament(form.value).subscribe((res) => {
      console.log(res);
      this.toolsService.openSnackBar('Médicament ajouté avec succès !')
      console.log('Médicament ajouté avec succès');
    },
    (err) => {
      console.log(err);
      this.toolsService.openSnackBar(err.error.text);
    }
    );
  }

}
