import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DialogAddMedicamentComponent } from './dialog-add-medicament.component';

describe('DialogAddMedicamentComponent', () => {
  let component: DialogAddMedicamentComponent;
  let fixture: ComponentFixture<DialogAddMedicamentComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogAddMedicamentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogAddMedicamentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
