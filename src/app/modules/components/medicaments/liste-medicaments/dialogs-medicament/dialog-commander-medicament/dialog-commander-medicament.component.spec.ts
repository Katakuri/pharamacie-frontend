import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DialogCommanderMedicamentComponent } from './dialog-commander-medicament.component';

describe('DialogCommanderMedicamentComponent', () => {
  let component: DialogCommanderMedicamentComponent;
  let fixture: ComponentFixture<DialogCommanderMedicamentComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogCommanderMedicamentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogCommanderMedicamentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
