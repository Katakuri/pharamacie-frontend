import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Medicament } from 'src/app/model/medicamentInterface';
import { MatDialogRef } from '@angular/material/dialog';
import { ToolsService } from 'src/app/shared/tools/tools.service';

@Component({
  selector: 'app-dialog-commander-medicament',
  templateUrl: './dialog-commander-medicament.component.html',
  styleUrls: ['./dialog-commander-medicament.component.scss']
})
export class DialogCommanderMedicamentComponent implements OnInit {

  form: FormGroup;
  medicament: Medicament
  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<DialogCommanderMedicamentComponent>,
    public tools : ToolsService
  ) { }

  ngOnInit() {
    console.log(this.data)
    this.medicament = {
      codeBarre: this.data.codeBarre,
      datePeremption: this.data.datePeremption,
      fournisseur: this.data.fournisseur,
      nom: this.data.nom,
      prix: this.data.prix,
      quantite: this.data.quantite
    }

    this.form = new FormGroup({
      idCommande : new FormControl(this.tools.ID(),[Validators.required]), 
      nom: new FormControl(this.medicament.nom, [Validators.required, Validators.maxLength(15)]),
      nbrMed: new FormControl('', [Validators.required, Validators.maxLength(15)]),
      codeBarre: new FormControl(this.medicament.codeBarre, [Validators.required, Validators.maxLength(15)]),
      prix: new FormControl(this.data.prix, [Validators.required]),
      fournisseur: new FormControl(this.data.fournisseur.nomFournisseur, [Validators.required])
    });
  }



  onNoClick() {
    this.dialogRef.close();
  }

  submit(form) {
    this.dialogRef.close(form.value)
  }

}
