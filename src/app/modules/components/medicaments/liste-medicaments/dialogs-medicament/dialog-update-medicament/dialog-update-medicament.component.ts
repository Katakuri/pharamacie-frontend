import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl} from '@angular/forms';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MedicamentsService } from 'src/app/shared/services/medicaments.service';
import { ToolsService } from 'src/app/shared/tools/tools.service';
import {Medicament} from "../../../../../../model/medicamentInterface"
import {FournisseurService} from "../../../../../../shared/services/fournisseur.service"
import { Fournisseur } from 'src/app/model/fournisseurInterface';

@Component({
  selector: 'app-dialog-update-medicament',
  templateUrl: './dialog-update-medicament.component.html',
  styleUrls: ['./dialog-update-medicament.component.scss']
})
export class DialogUpdateMedicamentComponent implements OnInit {

  form : FormGroup;
  medicament : Medicament;
  fournisseurs : Fournisseur[] = [];

  constructor(
  @Inject(MAT_DIALOG_DATA) public data : Medicament,
  public medicamentService : MedicamentsService,
  public toolsService : ToolsService,
  public dialogRef: MatDialogRef<DialogUpdateMedicamentComponent>,
  public fournisService : FournisseurService


  ) { }

  ngOnInit() {

    this.fournisService.getAllfournisseur().subscribe((res : Fournisseur[]) => {
      console.log(res);
      this.fournisseurs = res;
    },
    (err) => console.log(err));
    // console.log(this.data)
    this.medicament = {
      codeBarre : this.data.codeBarre,
      nom : this.data.nom,
      quantite : this.data.quantite,
      datePeremption : this.data.datePeremption,
      prix : this.data.prix,
      fournisseur : this.data.fournisseur
    };
    
    this.form = new FormGroup({
      nom : new FormControl(this.medicament.nom, [Validators.required, Validators.maxLength(15)]), 
      quantite : new FormControl(this.medicament.quantite, [Validators.required, Validators.maxLength(15)]),
      codeBarre : new FormControl(this.medicament.codeBarre, [Validators.required, Validators.maxLength(15)]),
      datePeremption : new FormControl(this.medicament.datePeremption, [Validators.required]),
      prix : new FormControl(this.data.prix, [Validators.required]),
      fournisseur : new FormControl(this.data.fournisseur, [Validators.required])
    });
  }

  onNoClick(){
    this.dialogRef.close();
  }

  submit(form){
    this.dialogRef.close(form.value)
  }

}
