import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DialogUpdateMedicamentComponent } from './dialog-update-medicament.component';

describe('DialogUpdateMedicamentComponent', () => {
  let component: DialogUpdateMedicamentComponent;
  let fixture: ComponentFixture<DialogUpdateMedicamentComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogUpdateMedicamentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogUpdateMedicamentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
