import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DialogDeleteMedicamentComponent } from './dialog-delete-medicament.component';

describe('DialogDeleteMedicamentComponent', () => {
  let component: DialogDeleteMedicamentComponent;
  let fixture: ComponentFixture<DialogDeleteMedicamentComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogDeleteMedicamentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogDeleteMedicamentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
