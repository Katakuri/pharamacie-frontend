import { Component, Inject, OnInit } from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {Medicament} from "../../../../../../model/medicamentInterface"

@Component({
  selector: 'app-dialog-delete-medicament',
  templateUrl: './dialog-delete-medicament.component.html',
  styleUrls: ['./dialog-delete-medicament.component.scss']
})
export class DialogDeleteMedicamentComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data : Medicament) { }
  medicament : Medicament;

  ngOnInit() {
    this.medicament = {
      codeBarre : this.data.codeBarre,
      nom : this.data.nom,
      quantite : this.data.quantite,
      datePeremption : this.data.datePeremption,
      prix : this.data.prix,
      fournisseur : this.data.fournisseur
    };
  }

}
