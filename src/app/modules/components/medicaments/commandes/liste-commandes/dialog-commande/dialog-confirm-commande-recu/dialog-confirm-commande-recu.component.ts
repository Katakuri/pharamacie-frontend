import { Component, Inject, OnInit } from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {Commande} from "../../../../../../../model/commandeInterface"

@Component({
  selector: 'app-dialog-confirm-commande-recu',
  templateUrl: './dialog-confirm-commande-recu.component.html',
  styleUrls: ['./dialog-confirm-commande-recu.component.scss']
})
export class DialogConfirmCommandeRecuComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data : Commande) { }
  commande : Commande;

  ngOnInit() {
    console.log(this.data);
  }

}
