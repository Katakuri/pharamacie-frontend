import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DialogConfirmCommandeRecuComponent } from './dialog-confirm-commande-recu.component';

describe('DialogConfirmCommandeRecuComponent', () => {
  let component: DialogConfirmCommandeRecuComponent;
  let fixture: ComponentFixture<DialogConfirmCommandeRecuComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogConfirmCommandeRecuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogConfirmCommandeRecuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
