import { Component, Inject, OnInit } from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {Commande} from "../../../../../../../model/commandeInterface"

@Component({
  selector: 'app-dialog-delete-commande',
  templateUrl: './dialog-delete-commande.component.html',
  styleUrls: ['./dialog-delete-commande.component.scss']
})
export class DialogDeleteCommandeComponent implements OnInit {

  commande : Commande;
  constructor(@Inject(MAT_DIALOG_DATA) public data : Commande) { }

  ngOnInit() {
    this.commande = {
      idCommande : this.data.idCommande,
      nbrMed : this.data.nbrMed,
      montant : this.data.montant,
      dateCommande : this.data.dateCommande,
      statut : this.data.statut
    }
  }

}
