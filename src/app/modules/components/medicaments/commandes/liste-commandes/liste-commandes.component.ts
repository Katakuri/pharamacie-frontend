import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Commande } from "../../../../../model/commandeInterface";
import {CommandeService} from "../../../../../shared/services/commande.service";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {DialogDeleteCommandeComponent} from "./dialog-commande/dialog-delete-commande/dialog-delete-commande.component"
import {ToolsService} from "../../../../../shared/tools/tools.service";
import { DialogConfirmCommandeRecuComponent } from './dialog-commande/dialog-confirm-commande-recu/dialog-confirm-commande-recu.component';
import {DialogUpdateMedicamentComponent} from "../../liste-medicaments/dialogs-medicament/dialog-update-medicament/dialog-update-medicament.component"
@Component({
  selector: 'app-liste-commandes',
  templateUrl: './liste-commandes.component.html',
  styleUrls: ['./liste-commandes.component.scss']
})
export class ListeCommandesComponent implements OnInit {
  displayedColumns: string[] = ['numCommande', 'medicament', 'fournisseur', 'dateCommande', 'nbMedicament', 'montant', 'statut','recu', 'saisir', 'supprimer'];
  dataSource: MatTableDataSource<Commande>;
  tabCommandes: Commande[] = [];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    public commandeServ : CommandeService,
    public dialog: MatDialog,
    public tools : ToolsService
  ) {}

  ngOnInit() {
    this.commandeServ.getAllCommandes().subscribe((res : Commande[]) => {
      console.log(res);
      this.tabCommandes = res;
      this.dataSource = new MatTableDataSource(this.tabCommandes);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },
    (err) => console.log(err));
  } 

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  openDeleteDialog(commande) {
    console.log(commande);
    let dialogRef = this.dialog.open(DialogDeleteCommandeComponent, {
      data: commande
    }).afterClosed().subscribe((res) => {
      console.log(res);
      if (res) {
        this.commandeServ.deleteCommande(commande.idCommande).subscribe(
          (res) => {
            this.tools.openSnackBar("La commande a bien été supprimé");
            this.ngOnInit();
          },
          (err) => this.tools.openSnackBar("Une erreur est survenu lors de la supression")
        )
      } else {
        this.tools.openSnackBar("Le medicament n'a pas été supprimé");
      }
    })
  }

  openChangeStatutDialog(commande) {
    console.log(commande);
    let dialogRef = this.dialog.open(DialogConfirmCommandeRecuComponent, {
      data : commande
    }).afterClosed().subscribe((res) => {
      console.log(res);
      if(res) {
        this.commandeServ.updateStatutCommande(commande.idCommande,commande).subscribe(
          (res) => {
            console.log(res);
            this.ngOnInit();
          },
          (err) => {
            console.log(err)
            this.ngOnInit();
          }
        )
      }else {
        console.log("KO");
      }
    })
  }

  openChangeStatutSaisieDialog(commande) {
    console.log(commande.medicament);
    let dialogRef = this.dialog.open(DialogUpdateMedicamentComponent, {
      data : commande.medicament
    }).afterClosed().subscribe((res) => {
      console.log(res);
      if(res) {
        this.commandeServ.updateStatutCommandeSaisie(commande.idCommande,commande).subscribe(
          (res) => {
            console.log(res);
            this.ngOnInit();
          },
          (err) => {
            console.log(err)
            this.ngOnInit();
          }
        )
      }else {
        console.log("KO");
      }
    })
  }

}
