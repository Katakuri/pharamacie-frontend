import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { ToolsService } from "../../../shared/tools/tools.service";
import {AuthService} from "../../../shared/services/auth.service"
import { Router } from '@angular/router';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  form: FormGroup;

  constructor(
    public tools : ToolsService,
    public authService : AuthService,
    public router : Router

  ) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      idUtilisateur: new FormControl(this.tools.ID(), [Validators.required, Validators.maxLength(15)]),
      nom: new FormControl('', [Validators.required, Validators.maxLength(15)]),
      prenom: new FormControl('', [Validators.required, Validators.maxLength(15)]),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required]),
      enable: new FormControl(true, [Validators.required])
    });
  }

  submit(form){
    this.authService.register(form.value).subscribe((res) => {
      this.router.navigate(['/home']);
    },
    (err) => console.log(err));
  }
}
