import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../../shared/services/auth.service'
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { ToolsService } from "../../../shared/tools/tools.service";
import { User } from 'src/app/model/userInterface';
@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.scss']
})
export class ProfilComponent implements OnInit {

  form: FormGroup;
  user : User;

  constructor(
    public profilInfo : AuthService,
  ) { }

  ngOnInit(): void {
    this.profilInfo.userProfil().subscribe((res : User) => {
      console.log(res);
      this.user = {
        idUtilisateur : res.idUtilisateur,
        nom : res.nom,
        prenom : res.prenom,
        email : res.email,
        password : res.password,
        enable : res.enable
      }

      this.form = new FormGroup({
        idUtilisateur: new FormControl(this.user.idUtilisateur, [Validators.required, Validators.maxLength(15)]),
        nom: new FormControl(this.user.nom, [Validators.required, Validators.maxLength(15)]),
        prenom: new FormControl(this.user.prenom, [Validators.required, Validators.maxLength(15)]),
        email: new FormControl(this.user.email, [Validators.required, Validators.email]),
        password: new FormControl(this.user.password, [Validators.required]),
        enable: new FormControl(true, [Validators.required])
      });
    },
    (err) => console.log(err));


  }

}
