import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Fournisseur } from "../../../../model/fournisseurInterface";
import { FournisseurService } from "../../../../shared/services/fournisseur.service";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogAddFournisseurComponent } from "../liste-fournisseurs/dialog-fournisseur/dialog-add-fournisseur/dialog-add-fournisseur.component"
import { DialogSendMailComponent } from './dialog-fournisseur/dialog-send-mail/dialog-send-mail.component';
import { ToolsService } from 'src/app/shared/tools/tools.service';
@Component({
  selector: 'app-liste-fournisseurs',
  templateUrl: './liste-fournisseurs.component.html',
  styleUrls: ['./liste-fournisseurs.component.scss']
})
export class ListeFournisseursComponent implements OnInit {
  displayedColumns: string[] = ['idFournisseur', 'nomFournisseur', 'mail', 'contacter'];
  dataSource: MatTableDataSource<Fournisseur>;
  tabFournisseurs: any[] = [];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    public fournisseurService: FournisseurService,
    public dialog: MatDialog,
    public tools: ToolsService

  ) { }

  ngOnInit() {
    this.fournisseurService.getAllfournisseur().subscribe((res: any) => {
      console.log(res);
      this.tabFournisseurs = res;
      this.dataSource = new MatTableDataSource(this.tabFournisseurs);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },
      (err) => console.log(err));
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  openDialogAddFournisseur() {
    this.dialog.open(DialogAddFournisseurComponent, {
      width: "380px"
    }).afterClosed().subscribe(
      (res: Fournisseur) => {
        console.log(res);
        this.fournisseurService.addFournisseur(res).subscribe((res) => {
          console.log(res);
          this.ngOnInit();
        },
          (err) => {
            console.log(err);
            this.ngOnInit();
          },
          () => this.ngOnInit());
      });
  }

  openDialogMailFournisseur(fournisseur) {
    console.log(fournisseur)
    this.dialog.open(DialogSendMailComponent, {
      width: "680px",
      data: fournisseur
    }).afterClosed().subscribe((res: any) => {
      if (res) {
        this.fournisseurService.mailFournisseur(fournisseur.idFournisseur, res.mailContent).subscribe(
          (res) => {
            console.log(res);
            this.tools.openSnackBar('Mail envoyé avec succès au fournisseur !')
          },
          (err) => this.tools.openSnackBar('Mail envoyé avec succès au fournisseur !')

        );
      }
    })
  }
}
