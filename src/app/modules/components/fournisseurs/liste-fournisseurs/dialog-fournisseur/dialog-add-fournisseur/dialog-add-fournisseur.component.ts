import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl} from '@angular/forms';
import {FournisseurService} from "../../../../../../shared/services/fournisseur.service"

@Component({
  selector: 'app-dialog-add-fournisseur',
  templateUrl: './dialog-add-fournisseur.component.html',
  styleUrls: ['./dialog-add-fournisseur.component.scss']
})
export class DialogAddFournisseurComponent implements OnInit {

  form : FormGroup;
  constructor(
    public fournisService : FournisseurService
  ) { }
  
  ngOnInit() {
    this.form = new FormGroup({
      nomFournisseur : new FormControl('',[Validators.required, Validators.maxLength(15)]), 
      idFournisseur : new FormControl('', [Validators.required, Validators.maxLength(15)]),
      mail : new FormControl('', [Validators.required, Validators.email])
    });
  }

}
