import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, NgModel, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-dialog-send-mail',
  templateUrl: './dialog-send-mail.component.html',
  styleUrls: ['./dialog-send-mail.component.scss']
})
export class DialogSendMailComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data : any,
  ) { }
  mailContent : String;
  form : FormGroup;




  ngOnInit(): void {
    console.log(this.data);
    this.form = new FormGroup({
      mailContent : new FormControl('', [Validators.required])
    });
  }

} 
