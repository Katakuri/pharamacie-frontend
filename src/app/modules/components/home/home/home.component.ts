import { Component, OnInit } from '@angular/core';
import { MedicamentsService } from "../../../../shared/services/medicaments.service";
import { AuthService } from "../../../../shared/services/auth.service";
import { Router } from '@angular/router';
import {CommandeService} from '../../../../shared/services/commande.service';
import { FournisseurService } from 'src/app/shared/services/fournisseur.service';
import { User } from 'src/app/model/userInterface';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  medicamentExp : Number;
  stockNb : Number;
  medicamentMin : Number;
  commandeEnCours : Number;
  commandeRecues : Number; 
  fournisseur : Number
  id : any;
  user : User;

  constructor(
    public medicamentServ : MedicamentsService ,
    public commandeServ : CommandeService,
    public fournisseurService : FournisseurService,
    public authService : AuthService,
    public router : Router
  ) { }

  ngOnInit() {
    this.medicamentServ.getMedicamentExp().subscribe((res : Number) => this.medicamentExp = res,
    (err) => console.log(err));

    this.medicamentServ.getNbStock().subscribe((res : Number) => this.stockNb = res,
    (err) => console.log(err));

    this.medicamentServ.getNbMedicamentMin().subscribe((res : Number) => this.medicamentMin = res,
    (err) => console.log(err));

    this.commandeServ.getNbCommandes().subscribe((res : Number) => this.commandeEnCours = res,
    (err) => console.log(err));

    this.commandeServ.getCommandesRecus().subscribe((res : Number) => this.commandeRecues = res,
    (err) => console.log(err));

    this.fournisseurService.getAllfournisseur().subscribe((res : []) => {
      this.fournisseur = res.length
    },
    (err) => console.log(err));

    this.authService.userProfil().subscribe((res : User) => {
      console.log(res);
      this.user = res
    },
    (err) => console.log(err));
  }

  logout(){
    localStorage.removeItem('id_token');
    this.router.navigate(['/']);
  }

}
