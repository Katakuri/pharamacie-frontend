import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl} from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import {AuthService} from "../../../shared/services/auth.service"
import { ToolsService } from 'src/app/shared/tools/tools.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: FormGroup;
  errorLogin = false;

  constructor(
    public http: HttpClient,
    private router: Router,
    private auth : AuthService,
    public tools : ToolsService
  ) { }

  ngOnInit() {
    this.form = new FormGroup({
      email : new FormControl('', [Validators.required, Validators.email, Validators.maxLength(30)]),
      password : new FormControl('', [Validators.required, Validators.maxLength(15)]),
    });
  }


  login(authentification) {
    localStorage.setItem('id_token', authentification.token);
  }


  submit(form) {
    console.log(form.value);
    this.auth.login(form.value).subscribe((res) =>{
      console.log(res);
      this.login(res);
      this.tools.openSnackBar('Utilisateur connecté !')
    },
    (err) => {
      console.log(err);
      this.tools.openSnackBar('Erreur mail ou mot de passe')
    },
    () => {
      this.router.navigate(['/home']);
    });
  }


}
