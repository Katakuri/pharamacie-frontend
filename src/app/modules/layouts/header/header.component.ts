import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {AuthService} from "../../../shared/services/auth.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(
    public authService : AuthService,
    public router : Router

  ) {}

  ngOnInit() {
  }

  isLogged() : Boolean{
    return this.authService.isUserLogged();
  }

  logout(){
    localStorage.removeItem('id_token');
    this.router.navigate(['/']);
  }
}
